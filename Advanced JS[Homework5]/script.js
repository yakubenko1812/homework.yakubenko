const button = document.getElementById('button');
const textContainer = document.getElementById('text_container');

button.addEventListener('click', async (e) => {
    button.setAttribute('disabled', 'disabled');

    const Request = await fetch('https://api.ipify.org/?format=json');
    const addressIP = await Request.json();


    const Request2 = await fetch(`http://ip-api.com/json/${addressIP.ip}`);
    const userInfo = await Request2.json();

    const {continent, country, regionName, city, zip, district} = userInfo;
    const continentEl = document.createElement('p');
    const countryEl = document.createElement('p');
    const regionNameEl = document.createElement('p');
    const cityEl = document.createElement('p');
    const zipEl = document.createElement('p');
    const districtEl = document.createElement('p');
    const container = document.createElement('div');

    continentEl.textContent = continent;
    countryEl.textContent = country;
    regionNameEl.textContent = regionName;
    cityEl.textContent = city;
    zipEl.textContent = zip;
    districtEl.textContent = district;


    container.append(continentEl, countryEl, regionNameEl, cityEl, zipEl, districtEl);
    textContainer.prepend(container);
})
