let timer;
let left = 0;
function autoSlider() {
    timer = setTimeout(function () {
        let slider = document.getElementById('images-wrapper');
        left = left - 400;
        if (left < -1200) {
            left = 0;
            clearTimeout(timer);
        }
        slider.style.left = left + 'px';
        autoSlider()
    }, 10000)
}
autoSlider();

let playing = true;
const pauseButton = document.getElementById('pause');
const playButton = document.getElementById('play');

pauseButton.onclick = function pauseSlideShow(){
    playing = false;
    clearInterval(timer);
}
playButton.onclick = function playSlideShow (){
    playing = true;
    timer = autoSlider()
}

