export const LOAD_PRODUCTS_LIST = "CHECK_SERVER";

export function getProductsFromServer() {
    return (dispatch) => {
        fetch("productsList.json")
            .then(res => res.json())
            .then(data => {
                dispatch({type: LOAD_PRODUCTS_LIST, payload: data})
            })
    }
}
