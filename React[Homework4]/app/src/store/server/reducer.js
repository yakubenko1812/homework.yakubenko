import initialStore from "../initialStore";
import {LOAD_PRODUCTS_LIST} from "./actions";

export default function reducer(productsFromStore = initialStore.productsList, {type, payload}) {
    switch (type) {
        case LOAD_PRODUCTS_LIST:
            return [
                ...productsFromStore,
                ...payload
            ];
        default:
            return productsFromStore
    }
}
