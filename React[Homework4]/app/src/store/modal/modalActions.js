export const MODAL_TYPE = "MODAL_TYPE";
export const MODAL_TARGET = "MODAL_TARGET";

export function setModalType(status) {
    return (dispatch) => {
        dispatch({type: MODAL_TYPE, payload: status})
    }
}

export function setCurrentTargetProps(props) {
    return (dispatch) => {
        dispatch({type: MODAL_TARGET, payload: props})
    }
}
