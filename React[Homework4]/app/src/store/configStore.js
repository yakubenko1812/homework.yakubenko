import {applyMiddleware, createStore} from "redux";
import {composeWithDevTools} from "redux-devtools-extension";
import initialStore from "./initialStore";
import thunk from "redux-thunk";
import rootReducer from "./reducer/rootReducer";

const store = createStore(
    rootReducer,
    initialStore,
    composeWithDevTools(
        applyMiddleware(thunk)
    )
);

export default store;
