import React from 'react';
import ProductsList from "../../components/ProductList/index";

function MainPage(props){
    const {productsList, displayModal, closeModal} = props;

    return (
        <ProductsList
            itemClassName="product-item__container"
            productsList={productsList}
            displayModal={displayModal}
            closeModal={closeModal}
            inCart={false}
        />
    );
}

export default MainPage;
