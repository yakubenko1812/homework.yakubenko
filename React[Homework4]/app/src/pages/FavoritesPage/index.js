import React from 'react';
import ProductsList from "../../components/ProductList/index";

function FavoritesPage(props){
    const checkFavorites = () => {
        if (localStorage.getItem("favorites")) {
            return JSON.parse(localStorage.getItem("favorites"))
        }
        return []
    }

    const {displayModal} = props;
    return (
        <ProductsList
            itemClassName="product-item__container"
            productsList={checkFavorites()}
            displayModal={displayModal}
            inCart={false}
        />
    );
}

export default FavoritesPage;
