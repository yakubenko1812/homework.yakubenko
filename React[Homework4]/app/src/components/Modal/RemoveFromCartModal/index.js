import React from 'react';
import ModalTemplate from "../ModalTemplate/index"
import Button from "../../Button/index";
import { v4 as uuid_v4 } from "uuid";
import PropTypes from "prop-types";

function RemoveFromCartModal (props) {

    const deleteFromCart = (itemProps) => {
        let storageCart = JSON.parse(localStorage.getItem("cart"));
        let updatedStorageCart = storageCart.filter(e => e.article !== itemProps.article)
        localStorage.setItem("cart", JSON.stringify(updatedStorageCart))
    }

    const {closeModal, currentTargetProps} = props
    return (
        <ModalTemplate className="modal"
                       header="Remove this item from your Cart?"
                       text=""
                       closeButton={true}
                       closeModal={closeModal}
                       currentTargetProps={currentTargetProps}

                       actions={[
                           <Button className="btn btn__action-modal"
                                   text="Ok"
                                   backgroundColor="dodgerblue"
                                   key={uuid_v4()}
                                   onClick={() => {
                                       deleteFromCart(currentTargetProps)
                                       closeModal()
                                   }}
                           />,

                           <Button className="btn btn__action-modal"
                                   text="Cancel"
                                   key={uuid_v4()}
                                   onClick={() => {
                                       closeModal()
                                   }}
                           />
                       ]}
        />
    );
}

RemoveFromCartModal.propTypes = {
    closeModal: PropTypes.func.isRequired,
    currentTargetProps: PropTypes.object.isRequired
}

export default RemoveFromCartModal;
