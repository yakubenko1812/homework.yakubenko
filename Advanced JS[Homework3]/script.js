class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    };

    set name(name) {
        this._name = name;
    };
    get name() {
        return this._name;
    };

    set age(age) {
        this._age = age;
    };
    get age() {
        return this._age;
    };

    set salary(salary) {
        this._salary = salary;
    };
    get salary() {
        return this._salary;
    };
}


class Programmer extends Employee {
    constructor(name, age, salary, ...lang) {
        super(name, age, salary);
        this.lang = lang;
    };

    getSalary() {
        return parseFloat(this.salary) * 3;
    };
}

const Programmer1 = new Programmer('Ivan', 25, '2000$', 'English', 'Ukrainian');
console.log(Programmer1, );
console.log(Programmer1.getSalary());

const Programmer2 = new Programmer('Steve', 30, '3000$', 'English', 'Russian', 'French');
console.log(Programmer2);
console.log(Programmer2.getSalary());


