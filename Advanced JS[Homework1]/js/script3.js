// Задание 3
// У нас есть объект user:
//     const user1 = {
//         name: "John",
//         years: 30
//     };
// Напишите деструктурирующее присваивание, которое:
//
// свойство name присвоит в переменную name
// свойство years присвоит в переменную age
// свойство isAdmin присвоит в переменную isAdmin (false, если нет такого свойства)
//
// Выведите переменные на экран.

const user1 = {
    name: "John",
    years: 30
};

function userInfo({name: name, years: age, isAdmin = false}){

    alert(`User name: ${name}. Age: ${age}. Admin: - ${isAdmin}`);
}

userInfo(user1);