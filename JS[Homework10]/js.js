const togglePassword = document.querySelector('#togglePassword');
const password = document.getElementById('password');
const togglePassword2 = document.querySelector('#togglePassword2');
const password2 = document.getElementById('password2');

togglePassword.addEventListener('click', function (e) {
    const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
    password.setAttribute('type', type);
    this.classList.toggle('fa-eye-slash');
});

togglePassword2.addEventListener('click', function (e) {
    const type2 = password2.getAttribute('type') === 'password' ? 'text' : 'password';
    password2.setAttribute('type', type2);
    this.classList.toggle('fa-eye-slash');
});


const form = document.addEventListener('submit', e => {
    e.preventDefault();
    checkInputs();
});

let error = document.querySelector('.error');
function checkInputs(){
    const passwordValue = password.value
    const password2Value = password2.value

    if( passwordValue !== password2Value){
        error.innerHTML = "Нужно ввести одинаковые значения";
        error.className = "error active";

    } else{
        alert('You are welcome!')
    }

}