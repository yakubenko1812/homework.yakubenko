import React from 'react';
import ModalTemplate from "../ModalTemplate";
import Button from "../../Button";
import "./ModalSecond.scss";

class ModalTwo extends ModalTemplate {
    render() {
        return (
            <ModalTemplate className={"modal-second"}
                header={"Do you want to close this modal window?"}
                text={"This area for some text... "}
                closeButton={true}
                closeModal={this.props.closeModal}

                actions={[
                    <Button className={"btn btn__action-modal"}
                            text={"Yes"}
                            backgroundColor="lightsteelblue"
                            key={Math.random()}
                            onClick={() => {
                                this.props.closeModal()
                                console.log("Action YES!")
                            }}
                    />,

                    <Button className={"btn btn__action-modal"}
                            text={"No"}
                            backgroundColor="grey"
                            key={Math.random()}
                            onClick={() => {
                                this.props.closeModal()
                                console.log("Action NO!")
                            }}
                    />
                ]}
            />
        );
    }
}

export default ModalTwo;
