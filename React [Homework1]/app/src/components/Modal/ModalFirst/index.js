import React from 'react';
import ModalTemplate from "../ModalTemplate";
import Button from "../../Button";
import "./ModalFirst.scss"

class ModalOne extends ModalTemplate {
    render() {
        return (
            <ModalTemplate className={"modal-first"}
                           header={"Do you want to delete this file?"}
                           text={"Once you delete this file, it won’t be possible to undo this action?" +
                           "Are you sure you want to delete it?"}
                           closeButton={true}
                           closeModal={this.props.closeModal}

                           actions={[
                               <Button className={"btn btn__action-modal"}
                                       text="Ok"
                                       backgroundColor="#B3382C"
                                       key={Math.random()}
                                       onClick={() => {
                                           this.props.closeModal()
                                           console.log("Action Ok")
                                       }}
                               />,

                               <Button className={"btn btn__action-modal"}
                                       text={"Cancel"}
                                       backgroundColor="#B3382C"
                                       key={Math.random()}
                                       onClick={ () => {
                                           this.props.closeModal()
                                           console.log("Action Cancel")
                                       }}
                               />
                           ]}
            />
        );
    }
}

export default ModalOne;
