import React from 'react';
import './App.scss';
import Button from "./components/Button";
import ModalOne from "./components/Modal/ModalFirst";
import ModalTwo from "./components/Modal/ModalSecond";

class App extends React.Component {
    constructor(props){
        super(props);
        this.onDialogClose = this.onDialogClose.bind(this);
        this.state = {
            isDialogShow: false
        }
    }
    onDialogClose(){
        this.setState({
            isDialogShow:false
        })

    }
    state = {
        openModal: null
    }

    closeModal = () => {
        this.setState({openModal: "null"})
    }

    changeState = (modalNum) => {
        this.setState({openModal: modalNum})
    }

    render() {
        return (
            <div className="App">
                <header className="App__navbar">
                    <Button
                        className={"btn btn__create-modal"}
                        text={"open first modal"}
                        backgroundColor="ariel"
                        onClick={() => {
                            this.changeState("first")
                        }}
                    />

                    <Button
                        className={"btn btn__create-modal"}
                        text={"open second modal"}
                        backgroundColor="dodgerblue"
                        onClick={() => {
                            this.changeState("second")
                        }}
                    />
                </header>

                {this.state.openModal === "first" && <ModalOne closeModal={this.closeModal}/>}
                {this.state.openModal === "second" && <ModalTwo closeModal={this.closeModal}/>}
            </div>
        );
    }
}

export default App;





