document.getElementById('change-theme-btn').addEventListener('click', function () {
    let darkTheme = document.body.classList.toggle('dark-theme');
    localStorage.setItem('dark-theme', darkTheme);
});

if (JSON.parse(localStorage.getItem('dark-theme'))) {
    document.body.classList.add('dark-theme');
}
