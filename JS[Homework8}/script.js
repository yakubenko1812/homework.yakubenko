input.onblur = function() {
    if (isNaN(this.value) || this.value <= 0 ) {
        this.className = "error";
        error.innerHTML = 'Please enter correct price.';

    }
    else{

        let span1 = document.createElement("div");
        span1.innerHTML = 'Текущая цена: ' + this.value + '$';
        form.prepend(span1)
        this.className = "span";

        let button = document.createElement( "button");
        button.setAttribute("id", "remove");
        button.remove(span1);
        span1.append(button);
        button.innerHTML = 'X';

    }
}