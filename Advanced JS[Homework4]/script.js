function getFilms (){
    fetch('https://swapi.dev/api/films')
        .then(response => response.json())
        .then(films => {
            let container = document.getElementById('container')
            let ul = document.createElement('ul');
            document.body.append(container);
            document.body.append(ul);

            films.results.forEach(el => {
                const {characters, episode_id, title, opening_crawl } = el;
                let li = document.createElement('li');
                let filmTitle = document.createElement('h2');
                ul.append(li);
                li.append(filmTitle);
                filmTitle.append(title);
                li.insertAdjacentHTML('beforeend',
                    `<p class="episode-id">EPISODE # ${episode_id}</p>
                      <p class="description">DESCRIPTION _<br> ${opening_crawl}</p>
            <hr>`);

                characters.forEach(item => {
                    fetch(item)
                        .then(response => response.json())
                        .then(actor => {
                            filmTitle.insertAdjacentHTML('afterend', `Character - ${actor.name}<br>`);

                        })
                })
            })
        })
}

getFilms();