import React, {useEffect, useState} from 'react';
import './App.scss';
import AddToCartModal from "./components/Modal/AddToCartModal/index";
import RemoveFromCartModal from "./components/Modal/RemoveFromCartModal/index";
import Navigation from "./components/Navigation/index";
import AppRoutes from "./routes/AppRoutes";

function App(){

  const [openModal, setOpenModal] = useState(null)
  const [productsList, setProductsList] = useState([])
  const [currentTargetProps, setCurrentTargetProps] = useState({})

  const displayModal = (modalTitle, props) => {
    setOpenModal(modalTitle)
    setCurrentTargetProps(props)
  }

  const closeModal = () => {
    setOpenModal(null)
  }

  useEffect(() => {
    getProducts()
  }, [])

  const getProducts = () => {
    fetch("productsList.json")
        .then(res => res.json())
        .then(data => {

          setProductsList(data)
        })
  }

  const addToCart = () => {
    if (openModal === "cartModal") {
      return <AddToCartModal
          currentTargetProps={currentTargetProps}
          closeModal={closeModal}/>
    }
  }

  const removeFromCart = () => {
    if (openModal === "removeModal") {
      return <RemoveFromCartModal
          currentTargetProps={currentTargetProps}
          closeModal={closeModal}/>
    }
  }

  return (
      <div className="App">
        <Navigation/>
        <AppRoutes
            productsList={productsList}
            displayModal={displayModal}
            closeModal={closeModal}
        />
        {addToCart()}
        {removeFromCart()}
      </div>
  );
}

export default App;
