import React from 'react';
import ProductsList from "../../components/ProductList/index";

function CartPage(props){
    const checkCart = () => {
        if (localStorage.getItem("cart")) {
            return JSON.parse(localStorage.getItem("cart"))
        }
        return []
    }

    const {displayModal, closeModal} = props;
    return (
        <ProductsList
            itemClassName="product-item__cart-container"
            productsList={checkCart()}
            displayModal={displayModal}
            closeModal={closeModal}
            inCart={true}
        />
    );
}

export default CartPage;
