import React from 'react';
import {Link} from "react-router-dom";
import Button from "../Button/index";
import "./Navigation.scss"

function Navigation(){
    return (
        <header className="App__navbar">
            <div>
                <Link className="App__logo-text" to="/">Shop</Link>
            </div>
            <div>
                <Link to="/cart">
                    <Button
                        className="btn btn__show-cart"
                        text="Cart"
                        backgroundColor="green"
                        onClick={() => {
                        }}
                    />
                </Link>

                <Link to="/favorites">
                    <Button
                        className="btn btn__show-favorites"
                        text="Favorites"
                        backgroundColor="dodgerblue"
                        onClick={() => {
                        }}
                    />
                </Link>
            </div>
        </header>
    );
}

export default Navigation;
