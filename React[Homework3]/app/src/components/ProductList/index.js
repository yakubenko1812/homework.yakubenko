import React from 'react';
import "./ProductList.scss"
import ProductItem from "../ProductItem/index";
import PropTypes from "prop-types";

function ProductsList (props) {
    const {itemClassName: className, productsList, displayModal, closeModal, inCart} = props;

    return (
        <div className="products-list">
            {productsList.map(el => {
                return <ProductItem className={className}
                                    title={el.title}
                                    price={el.price}
                                    imageUrl={el.imageUrl}
                                    key={el.article}
                                    article={el.article}
                                    color={el.color}
                                    displayModal={displayModal}
                                    closeModal={closeModal}
                                    inCart={inCart}
                />})}
        </div>
    );
}

ProductsList.propTypes = {
    productsList: PropTypes.arrayOf(PropTypes.object).isRequired,
    displayModal: PropTypes.func.isRequired
}

export default ProductsList;



