import React from 'react';
import Button from "../index";

function CloseItemBtn (props)  {
    const {className, onClick, text} = props
    return (
        <Button
            className={className}
            text={text}
            backgroundColor=""
            onClick={onClick}
        />
    );
}

export default CloseItemBtn;
