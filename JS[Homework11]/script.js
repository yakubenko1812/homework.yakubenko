const doKeyIllumination = (e) => {
    let but = document.querySelectorAll('button');
    but.forEach((item) => {
        item.classList.remove('active');
        if (item.getAttribute('data-key') == e.keyCode) {
            item.classList.add('active');
        }
    });
};
window.addEventListener('keydown', doKeyIllumination);