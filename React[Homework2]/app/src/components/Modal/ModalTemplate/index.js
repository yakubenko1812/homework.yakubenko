import React, {Component} from 'react';
import Button from "../../Button/index";
import "./ModalTemplate.scss";
import PropTypes from "prop-types";

class ModalTemplate extends Component {
    render() {
        const { header, closeButton, actions, closeModal, currentTargetProps} = this.props;

        return (
            <div className="modal__container"
                 onClick={(e) => {
                     if (e.target === e.currentTarget) {
                         closeModal()
                     }
                 }}
            >
                <div className="modal-body">
                    <div className="modal__btn-close-container">
                        {closeButton && <Button
                            className="btn btn__modal--close"
                            text="+"
                            backgroundColor="whitesmoke"
                            onClick={() => {
                                closeModal()
                            }}
                        />
                        }
                    </div>
                    <h2>{header}</h2>

                    <div className="modal__product-box">
                        <div className="modal__product-box--img-box">
                            <img className="modal__product-box--img" alt={`${currentTargetProps.title}`}
                                 src={currentTargetProps.imageUrl}/>
                        </div>
                        <div className="modal__product-box--text-box">
                            <p>{currentTargetProps.title}</p>
                            <p>${currentTargetProps.price}</p>
                        </div>
                    </div>

                    <div>{actions}</div>
                </div>
            </div>
        );
    }
}

ModalTemplate.propTypes = {
    className: PropTypes.string.isRequired,
    header: PropTypes.string.isRequired,
    closeButton: PropTypes.bool.isRequired,
    actions: PropTypes.arrayOf(PropTypes.object).isRequired,
    closeModal: PropTypes.func.isRequired,
    currentTargetProps: PropTypes.object.isRequired
}

export default ModalTemplate;
