import React from 'react';
import ModalTemplate from "../ModalTemplate/index"
import Button from "../../Button/index";
import PropTypes from 'prop-types';
import { v4 as uuid_v4 } from "uuid";


class AddToCartModal extends ModalTemplate {

    addToCart(itemProps) {
        const storageCart = localStorage.getItem("cart");

        if(!storageCart) {
            localStorage.setItem("cart", JSON.stringify([itemProps]));
        } else {
            let updatedStorageCart = JSON.parse(localStorage.getItem("cart"));
            updatedStorageCart.push(itemProps);
            localStorage.setItem("cart", JSON.stringify(updatedStorageCart));
        }
    }

    render() {
        const {closeModal, currentTargetProps} = this.props
        return (
            <ModalTemplate className="modal"
                           header="Do you want add this item to your Cart?"
                           text=""
                           closeButton={true}
                           closeModal={closeModal}
                           currentTargetProps={currentTargetProps}

                           actions={[
                               <Button className="btn btn__action-modal"
                                       text="Ok"
                                       backgroundColor="lightgreen"
                                       key={uuid_v4()}
                                       onClick={() => {
                                           this.addToCart(currentTargetProps)
                                           closeModal()
                                       }}
                               />,

                               <Button className="btn btn__action-modal"
                                       text="Cancel"
                                       backgroundColor="lightblue"
                                       key={uuid_v4()}
                                       onClick={() => {
                                           closeModal()
                                       }}
                               />
                           ]}
            />
        );
    }
}

AddToCartModal.propTypes = {
    closeModal: PropTypes.func.isRequired,
    currentTargetProps: PropTypes.object.isRequired
}

export default AddToCartModal;
