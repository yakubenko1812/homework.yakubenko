import React, {Component} from 'react';
import "./ProductItem.scss"
import Button from "../Button/index";
import PropTypes from "prop-types";

class ProductItem extends Component {

    state = {
        inFavorites: false
    }

    addToFavorites(itemProps) {
        const storageFavorites = localStorage.getItem("favorites");

        if (!storageFavorites) {
            localStorage.setItem("favorites", JSON.stringify([itemProps]));
        }

        if (storageFavorites) {
            if (storageFavorites.includes(JSON.stringify(itemProps))) {
                let parsedStorageFavorites = JSON.parse(storageFavorites)
                let updatedStorageFavorites = parsedStorageFavorites.filter(e => e.article !== itemProps.article);
                localStorage.setItem("favorites", JSON.stringify(updatedStorageFavorites))
            } else {
                let updatedStorageFavorites = JSON.parse(localStorage.getItem("favorites"));
                updatedStorageFavorites.push(itemProps);
                localStorage.setItem("favorites", JSON.stringify(updatedStorageFavorites));
            }
        }
    }

    render() {
        const {className, title, price, imageUrl, displayModal} = this.props

        return (
            <div className={className}>
                <div className="product-item__title">
                    <h4>{title}</h4>
                    <h5>${price}</h5>
                </div>
                <div>
                    <img className="product-item-img" src={imageUrl} alt={`${title}`}/>
                </div>
                <div className="product-item__buttons-container">
                    <Button
                        className="btn btn__add-to-cart"
                        text="add to cart"
                        backgroundColor="green"
                        onClick={() => {
                            displayModal("cartModal", this.props)
                        }}
                    />
                    <img
                        className="img__add-to-favorites"
                        alt="add-to-favorites"
                        src={this.state.inFavorites ? "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOYAAADbCAMAAABOUB36AAAAhFBMVEX///8pbCkJYQnR29EAXgAAXwAmayYeZx4iaSISYxIZZhn4+vgAXADf598cZxwjaSPt8u2mvKa7zLuOq453m3dei14OYg4ycTJGfEZQglCuwq7X4df09/SctZzB0MF9n305dTlmkGbJ1smetp5tlG2GpYbd5d0AVQBVhVVCekK0x7Q4dDgQJdxMAAAHvUlEQVR4nN2diULqMBBFDdDFIsiioIIgbs/l///vFUSgdNJmYmYmyfmA50Tv6+1yc3Nxwc5ywf8zBZhMpSdg4VVJT8BC0htIj8DAalY8Ss/AwE2WPknPwMBMqSR+1T53lCpepKcgZz1U6vpOegpyilQp1RtJj0FMt9Rsqdqx9BzEvA23y8xjV+1Os9GrtpvsVqkuH6QnIWU6/Flm/iE9CSnqR7PlHcJSehRCFnvNlqr9kp6FkGnxu8z8VnoWQua/mi2vtfGq9v2g2ahV+1gcl5lvpKch4/uo2VK1felxiBicaFap2UR6HiJeitNlZq/S8xDxeX26TNWRnoeGUa+ySjVbSU9EwrioLjO7kZ6IhLuqZiNV7blmy2U+S89EwMPl+TKHa+mZCLg/12x5wyc9k3vqmo1StXXNlqp9k57KObd5fZlpJj2Va5ZJfZVKJV3puRzzBWg2QtVuAM2Wqs2l53JLH7jO7lQbV9xiAmpWqSKuuMUmg5eZRhW36IPX2Z1q36Vnc8hqpltmVHGLV41mS9V+S8/mkI5ulUpFFBJ6blhmRKq90Wq2VO2T9HTOaPhjRqTaJs1GFLdYD5uWGU1IaJg2LTOWuEW3UbNKXcah2rdGzZaqvZee0Al5s2YjUe1Ce9t+UG0MIaFpi2YjCQmpNs1GEbd4b9VsFHGLadG+zAhCQvN2zSqVhB63GBhoNoK4xaOBZpXKQg8JPZloNviQ0EDzGrqm2rDjFi9Gmg0+JPRZ/0QNE3TcYmR0nQ1etedRoAbVhhwSAmIVOgKOW0CxCh0Bxy2gWIWOgENCCM2WFyHpaW1ZXiFWqTqhxi0wmg04bgFFgfSEGreAo0B6Ag0JwVEgPcMw4xZwFEhPmHELfaxCR5AhoYk2VqEjyJCQPlahI0jVojUbZEhIHwXSE2DcAq9Z0rjFoktD6wciiM6KZpjFxfL+X4cCm1WWD9cUo/y7336KGvcwz0vBcd3bf/QfzI1f2YRHMT/mjm4QbzPCIqm8Spt0kDegYZB3zr5Bje6QzxMhcHlXD29Me2ZfdYIh7YH3yN3MwtH9JRtqHtP7txb3ob7SudV/S4zGQg9mCTP4jsJCT80SJgYL7Rl8d1qFbqE1s4QZ3Vk8KvoDZJYwAVuoxixhurnV46I8WrOE6W+CtNAms4R5CM9CW8wSJjgLbTdLmHVQVyITs4RZzYKx0Dz5Q7pxdB+IhZqbJczjVQDCRZklzMJ/C0WaJYz3Foo3SxivLTS1MUuYwZO3FmprljC+Wqi9WcJ4+RT6J7OEWfpnoX81SxjPLDS9IooqLFKPLNSJWcJ4ZKGuzBLGEwt1aJYwIx8s1K1ZwshbqGuzhFnNRD8pEZgljKiF0pgljJiFkpkljJCFEpqlhlcBC6U1S5iHHvPNPLlZwjBbKIdZwrwxWiiPWcI8d5gslM0sYZYfLBbKaZYwDBbKbJYwC0VsofxmCUNroRJmCfNF9xQqZJYwI+Md8dhVipklDNH/T8+OGzNpBbLBsyYhs4YVPJ4d3GTUCmSDV01CZq1ANnjVJESlWc86WT7pbvg6/qgW01aBxaNOFvOGFTwedbLUDltySSK9ul8oNetRJ8uYdKdO5ksnC6phBY8nTULmTVZ2eKJaXMMKHk+ahD6IX0unQ+kVblmSb1zxopMF27CCx4smIVwrkA0+qFZ32JJLPOhk0R225BIPmoR0hy25RL6TBd8KZIN4J4tNwwoe8U4Wm4YVPOlceJmW3SNYEtmX7zyaFVdt02FLLhE+uIlJs8IHNzUftuSS4kVwmc2HLblE9OCmwuY1dGb1uxE8AqftsCWQZP1oEycSPG6s7bAlgF3A53mIv0ALHtyUof8q+4BP/xavAzHVdrG37Sf7LMdo4Yod3NR+2FKVSsAH3ZN2LXVwU4r7g5wHfG6QYhA6uKn9gLBTgIDPJEG9RhKKW5gctnQATMPietKEDm7CxCoSzVdKVMmLSEgIEQVqaPDpIixURLXmsYrGNCzCQkUObjLVbGspgXlPmsDBTaaHLRmkYY0tVCAkZHjYklka1rAnTSAkZBQtNU7DGloo+8FNRp+oEVtHzCyUPSRkEgXSmSWMiYWyh4Tao0CGdXdHTKpGmY/AaY8CWWwdMbBQ5rhFWxTIssGn1UKZQ0ItUSDrrSOtFsp6rW3R7F+2jqxb/mlO1TZGgf64daS55IU1btEUBfrzPstGC00zNyswoemwJZxZwkwbttcxhoT0USBH+yy7+jfzjKrVHrbkbJ9lf6O7EqWpm59gMINmBAd1d0e0JS9sISHNYUuO91nqqkbZDm6Co0Du91nCFsoVEgKjQCT7LGELZQoJQbEKolICsGqUKW4BRIHoSgmAHerpN9UPq1D7BZOWEgBt3Sxxi1qsgriUoN6TxqLasyiQU7OEObdQlpBQ9Y+ZFQz3mOcWyqDaqma5SgmqPWkMcYvTKBBjKUHFQhlCQidRINYGn0pPWkIdtziJAnE3+JxY6CW1ig5RIIEGn2NbN3lIKN//RkUafI4WShwS2scqGMwS5tdCiUNCP1EgFrOE2fek5bQhoZ1mZRt8fiz0ijJusdWseIPPatuTRhq3mBaCdXcHthZKGhKaX0vW3R0pLZSwk2VwJVt3d2SR9ugmmT5J190d2dBdayV3D9QYY661/wHEv4cKVdVrngAAAABJRU5ErkJggg=="
                            : "https://d29fhpw069ctt2.cloudfront.net/icon/image/38027/preview.svg"}
                        onClick={() => {
                            this.setState({inFavorites: !this.state.inFavorites})
                            this.addToFavorites(this.props)
                        }}
                    />
                </div>
            </div>
        );
    }
}

ProductItem.propTypes = {
    className: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    imageUrl: PropTypes.string.isRequired,
    displayModal: PropTypes.func.isRequired,
    article: PropTypes.number.isRequired,
    color: PropTypes.string
}

export default ProductItem;
