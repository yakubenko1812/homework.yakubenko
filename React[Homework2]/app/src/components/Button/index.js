import React, {Component} from 'react';
import "./Button.scss";
import PropTypes from 'prop-types';

class Button extends Component {
    render() {
        const {className, text, backgroundColor, onClick} = this.props
        return (
            <button className={className}
                    style={{backgroundColor: backgroundColor}}
                    onClick={onClick}
            >{text}</button>
        );
    }
}

Button.propTypes = {
    className: PropTypes.string.isRequired,
    text: PropTypes.string,
    backgroundColor: PropTypes.string,
    onClick: PropTypes.func.isRequired
}

export default Button;
