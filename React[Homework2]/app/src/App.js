import React, {Component} from 'react';
import './App.scss';
import Button from './components/Button/index';
import AddToCartModal from "./components/Modal/AddToCartModal/index";
import ProductsList from "./components/ProductList";

class App extends Component {

  state = {
    openModal: null,
    productsList: [],
    currentTargetProps: {}
  }

  displayModal = (modalTitle, props) => {
    this.setState({openModal: modalTitle, currentTargetProps: props})
  }

  closeModal = () => {
    this.setState({openModal: "null"})
  }

  componentDidMount() {
    this.getProducts()
  }

  getProducts() {
    fetch("productsList.json")
        .then(res => res.json())
        .then(data => this.setState({productsList: data}))
  }

  render() {

    return (
        <div className="App">

          <header className="App__navbar">
            <div>
              <a className="App__logo-text" href="#">Shop</a>
            </div>
            <div>
              <Button
                  className="btn btn__show-cart"
                  text="Cart"
                  backgroundColor="green"
                  onClick={() => {}}
              />

              <Button
                  className="btn btn__show-favorites"
                  text="Favorites"
                  backgroundColor="dodgerblue"
                  onClick={() => {}}
              />
            </div>
          </header>

          <ProductsList
              productsList={this.state.productsList}
              displayModal={this.displayModal}
          />

          {this.state.openModal === "cartModal" && <AddToCartModal
              currentTargetProps={this.state.currentTargetProps}
              closeModal={this.closeModal}/>}

        </div>
    );
  }
}

export default App;
